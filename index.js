var blklab = require('blklab');
var config = require('./config');

var env = process.argv.pop();
var server = blklab.init(config);
server.listen(4010);

module.exports = server;
