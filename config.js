var routes = require('./application/routes/index');

module.exports = {
	sitename: 'imageprocessor',
	routes: routes,
	static: __dirname + '/public',
	uploads: __dirname + '/data/uploads/',
	views: __dirname + '/application/views/'
}
