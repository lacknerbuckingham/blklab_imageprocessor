//imports
var templates = require('./templates')
var def = require('./default')

module.exports = {
	templates: {"path":"/api/templates", "module":templates},
	'default': {"path":"/", "module":def},
}
