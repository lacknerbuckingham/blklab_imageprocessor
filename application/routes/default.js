var def = require('../controllers/default');
var express = require('express');
var router = express.Router();

router.route('/').get(def.home);
router.route('/req').get(def.req);
router.route('/images/upload').post(def.upload);
router.route('/videos/upload').post(def.upload_video);
router.route('/documents/upload').post(def.upload_document);

module.exports = router;
