var templates = require('../controllers/templates');
var express = require('express');
var router = express.Router();

router.route('/:id').get(templates.single);
router.route('/system/:id').get(templates.system);

module.exports = router;
