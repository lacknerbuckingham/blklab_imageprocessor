var model = require('../models/default');
var fs = require('fs-extra');
var uuid = require('node-uuid');
var sys = require('sys');
var exec = require('child_process').exec;
var sharp = require('sharp');
var path = require('path');
var os = require('os');
var pkgcloud = require('pkgcloud');
var moment = require('moment');

var client = pkgcloud.storage.createClient({
  provider: 'rackspace',
  username: 'bairdb',
  apiKey: 'cf7f82229fba584820efd5d050b7067b',
  region: 'ORD'
});

var redis = require("redis"),
    redis_client = redis.createClient();

sharp.cache(20);

var uris = {}

var getURI = function(origin, callback){
    var uri = redis_client.get(origin, function(err, uri){
        if(uri){
            callback(uri);
        }else{
            /*client.getContainer(origin, function(err, container) {
                redis_client.set(origin, container.cdnUri);
                callback(container.cdnUri);
            });*/
            callback(uri)
        }
    });
}

var controllers = {

    home: function(req, res) {
        var data = {
            title: req.params.id
        };
        res.render('index', data);
    },

    upload: function(req, res){
        var fields = {};
        var files = [];

        var mimes = {
            "image/jpeg": '.jpg',
            "image/png": '.png',
            "image/gif": '.gif',
            "image/svg+xml": '.svg'

        };

        var config = require('../../config');
        var fstream;

        var origin = req.headers.origin.replace('http://', '');
        var uri;
        getURI(origin, function(uri) {
            req.busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
                var base_name = uuid.v1();
                fields[fieldname] = filename;
                var ext = mimes[mimetype];

                var large = base_name + '-lg' + ext;
                var medium = base_name + '-md' + ext;
                var small = base_name + '-sm' + ext;
                var thumb = base_name + '-thumb' + ext;
                var original = base_name + ext;
                var path = config.uploads + original;
                var output_path = config.static + '/assets/images/';

                //REMOVE
                uri = 'http://imageprocessor.lackner-buckingham.com/assets/images/';

                var data = {'uri': uri, 'filename': base_name, 'extension': ext, 'domain': origin, 'original': filename, 'tmp': 'http://imageprocessor.lackner-buckingham.com/assets/images/' + large, 'url': uri + '/' + filename + ext, 'timestamp': moment().unix()};
                files.push(data);

                var params = {
                    props: data
                }

                model.saveMedia(params, origin).done();

                var file_names = [large, medium, small, thumb];

                fstream = fs.createWriteStream(path);
                file.pipe(fstream);
                fstream.on('close', function () {

                if(ext != '.gif' && ext != '.svg'){
                    var orig = sharp(path);
                    orig.rotate().resize(2000,null).max().withoutEnlargement().toFile(output_path + large).then(function(){
                        orig = sharp(path);
                        orig.rotate().resize(1200,null).max().withoutEnlargement().toFile(output_path + medium).then(function(){
                            orig = sharp(path);
                            orig.rotate().resize(640,null).max().withoutEnlargement().toFile(output_path + small).then(function(){
                                orig = sharp(path);
                                orig.rotate().resize(400,400).crop(sharp.gravity.center).toFile(output_path + thumb).then(function(){
                                    fs.unlink('/var/www/processor/data/uploads/' + base_name + ext);
                                    orig = null;
                                    fstream = null;
                                });
                            });
                        });
                    });
                }else{
                    fs.createReadStream(path).pipe(fs.createWriteStream(output_path + large))
                    fs.createReadStream(path).pipe(fs.createWriteStream(output_path + medium))
                    fs.createReadStream(path).pipe(fs.createWriteStream(output_path + small))
                    fs.createReadStream(path).pipe(fs.createWriteStream(output_path + thumb));
                    fs.unlink('/var/www/processor/data/uploads/' + base_name + ext);
                }

                    /*orig.rotate().resize(2000, 2000).max().withoutEnlargement().toFile(output_path + large).then(function(err) {
                        if(err){
                            console.log(err);
                        }
                        orig.rotate().resize(1200, 1200).max().withoutEnlargement().toFile(output_path + medium, function(err) {
                            if(err){
                                console.log(err);
                            }

                            orig.rotate().resize(640, 640).max().withoutEnlargement().toFile(output_path + small, function(err) {
                                if(err){
                                    console.log(err);
                                }

                                orig.rotate().resize(400, 400).crop(sharp.gravity.center).toFile(output_path + thumb, function(err) {

                                    fs.unlink('/var/www/processor/data/uploads/' + filename);

                                    if(err){
                                        console.log(err);
                                    }

                                    var pipe = function(p, remote, callback){
                                        var source = fs.createReadStream(p);
                                        var dest = client.upload({
                                          container: origin,
                                          remote: remote
                                        });

                                        dest.on('success', callback);
                                        dest.on('error', function(err){
                                            console.log(err);
                                        });

                                        source.pipe(dest);
                                    }

                                    orig = null;

                                    var p = output_path + large;
                                    pipe(p, large, function(){
                                        p = output_path + medium;
                                        pipe(p, medium, function(){
                                            p = output_path + small;
                                            pipe(p, small, function(){
                                                p = output_path + thumb;
                                                pipe(p, thumb, function(){});
                                            });
                                        });
                                    });
                                });
                            });

                        });
                    });*/
                });
            });

            req.busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {
                fields[fieldname] = val;
            });

            req.busboy.on('finish', function(){
                var resp = {meta: {'uri': uri}, files: files}
                res.send(resp);
            });

            req.pipe(req.busboy);
        });
    },

    upload_video: function(req, res){
        var fields = {};
        var files = [];

        var mimes = {
            "video/mp4": '.mp4',
            "video/quicktime": '.mov',
            "video/x-msvideo": '.avi',
            "video/x-ms-wmv": '.wmv',
        };

        var config = require('../../config');
        var fstream;

        var origin = req.headers.origin.replace('http://', '');
        var uri;
        getURI(origin, function(uri) {
            req.busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
                fields[fieldname] = filename;
                var base_name = uuid.v1();
                var ext = mimes[mimetype];
                var original = base_name + ext;
                var path = config.uploads + original;
                var output_path = config.static + '/assets/videos/';

                var data = {'uri': uri, 'filename': base_name, 'extension': ext, 'domain': origin, 'original': filename, 'tmp': 'http://imageprocessor.lackner-buckingham.com/assets/videos/' + base_name + '.png', 'url': uri + '/' + filename + ext, 'timestamp': moment().unix()};
                files.push(data);

                var params = {
                    props: data
                }

                model.saveMedia(params, origin).done();

                fstream = fs.createWriteStream(output_path + original);
                file.pipe(fstream);
                fstream.on('error', function () {
                    console.log(arguments);
                });
                fstream.on('close', function () {
                    var child = exec('sh /var/www/processor/scripts/encode.sh ' + original + ' ' + base_name, function (error, stdout, stderr) {});
                });
            });

            req.busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {
                fields[fieldname] = val;
            });

            req.busboy.on('finish', function(){
                var resp = {meta: {'uri': uri}, files: files}
                res.send(resp);
            });

            req.pipe(req.busboy);
        });
    },

    upload_document: function(){

    },

    req: function(req, res){
        res.render('request', null);
    }
};

module.exports = controllers
