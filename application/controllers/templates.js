var Model = require('blklab').Model;
var templatesModel = require('../models/templates');

var controllers = {

	single: function(req, res) {
		res.render(req.params.id, null);
	},

	system: function(req, res) {
		var config = require('../../config');
		res.sendfile(config.views + req.params.id + '.html');
	},

};

module.exports = controllers
