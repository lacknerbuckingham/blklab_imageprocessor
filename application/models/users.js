var Model = require('blklab').Model;
var util = require('util');

var usersModel = Model.extend({});

var model = new usersModel({
	db:'imageprocessor',
	collection: 'users'
});

model.schema({
	title: {type: 'String'},
	identifier: {type: 'String'},
	deleted: {type: 'Integer', 'default': 0}
});

module.exports = model;
