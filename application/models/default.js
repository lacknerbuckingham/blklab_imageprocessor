var Model = require('blklab').Neo4jModel;
var util = require('util');
var Promise = require('promise');

var Model = Model.extend({
	checkMedia: function(params, host){
		var self = this;
		return new Promise(function (resolve, reject){
			var query = [
				'MATCH (website:Website {domain:"' + host + '"})<-[:HASIMAGE]->(image)',
				'WHERE image.original={original}',
				'RETURN image'
			].join('\n')
			
			self.findOne(query, params, function(err, result) {				
				resolve(result);
			});
		});
	},
	
	saveMedia: function(params, host){
		var self = this;
		return new Promise(function (resolve, reject){
			var query = [
				'MATCH (website:Website {domain:"' + host + '"})',
				'CREATE (image:Image {props})',
				'CREATE (website)-[link:HASIMAGE {src: image.url}]->(image)',
				'RETURN image'
			].join('\n')

			self.insert(query, params, function(err, result) {				
				resolve(result);
			});
		});
	},
	
	loadAll: function(params, host){
		var self = this;
		var query = [
			'MATCH (image:Image)',
			'RETURN image'
		].join('\n')	
		
		self.load(query, {}, function(err, result) {
			console.log('test');
		});
		
		/*return new Promise(function (resolve, reject){
			self.load(query, {}, function(err, result) {
				console.log('test');
				resolve(result);
			});
		});*/
	}
});

var model = new Model({
	db:'imageprocessor',
	collection: 'default'
});

model.schema({
	title: {type: 'String'},
	identifier: {type: 'String'},
	deleted: {type: 'Integer', 'default': 0}
});

module.exports = model;
