(function (window, document) {
    'use strict';

	FileReader.prototype.__progress__ = null;

	var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
	Function.prototype.__observes__ = null;
	Function.prototype.__on__ = null;

	Function.prototype.observes = function () {
		this.__observes__ = Array.prototype.slice.call(arguments);
		return this;
	};

	Function.prototype.on = function () {
		this.__on__ = Array.prototype.slice.call(arguments);
		return this;
	};

	//Base object for extending the Coffea
	function N() {
		this.methods = {};
		this.collections = [];

		this.applyMethods = function (obj, methods) {
			var method;
			for(method in methods) {
				if (obj.hasOwnProperty(method) === false) {
					if(methods[method]){
						if (methods[method].__observes__) {
							this.observe(obj, methods[method]);
						}

						if(methods[method].__on__){
							this.on(obj, methods[method]);
						}
					}
					try{
						obj[method] = methods[method];
					}catch(e){}
					this.methods[method] = methods[method];
				}
			}
			return obj;
		};

		this.on = function(obj, method){
			var o = Coffea.find(obj);
			var prop = method.__on__;
			o.ev(prop, method);
		};

		this.observe = function (obj, method) {
			var prop = method.__observes__;
			var o;
			var oldval;
			var newval;
			if (prop == 'value' || prop == 'enter') {
				o = Coffea.find(obj);
				oldval = o.value;
				var ret = function(e){
					newval = o.value;
					method.bind(o).call(null, o, oldval, newval);
					oldval = newval;
				};

				if(prop == 'value'){
					o.bind('change', ret);
				}else{
					o.enter(ret);
				}
			} else if (prop == 'html') {
				o = Coffea.find(obj);
				oldval = o.html();
				o.on('html', function (e) {
					newval = o.html();
					method.bind(o).call(null, o, oldval, newval);
					oldval = newval;
				});
			} else if (prop == 'style' || prop == 'id' || prop == 'class') {
				var observer = new MutationObserver(function (mutations) {
					mutations.forEach(function (mutation) {
						console.log(mutation);
					});
				});

				observer.observe(obj, {
					attributes: true,
					childList: true,
					characterData: true
				});
			} else {
				obj.watch(prop, method);
			}
		};
	}

	//Expose the Coffea object
	window.Coffea = new N(document);

	//Shim for watching object for changes
	Function.prototype.watch = function (prop, handler) {
		var oldval = this[prop],
			newval = oldval,
			getter = function () {
				return newval;
			},
			setter = function (val) {
				oldval = newval;
				newval = handler.call(this, prop, oldval, val);
				return newval;
			};

		if (delete this[prop]) { // can't watch constants
			if (Object.defineProperty) { // ECMAScript 5
				Object.defineProperty(this, prop, {
					get: getter,
					set: setter
				});
			} else if (Object.prototype.__defineGetter__ && Object.prototype.__defineSetter__) { // legacy
				Object.prototype.__defineGetter__.call(this, prop, getter);
				Object.prototype.__defineSetter__.call(this, prop, setter);
			}
		}
		return this;
	};


    //Watching for Coffea read and onLoad...Needs more work
    var callbacks = [];
    var loadedCallbacks = [];
    var CoffeaLoaded = function () {
        //callbacks.forEach(Coffea.dispatch);
    };
    Coffea.ready = function (callback) {
        callbacks.push(callback);
    };

    var pageLoaded = function () {
        //loadedCallbacks.forEach(Coffea.dispatch);
    };
    Coffea.loaded = function (callback) {
        loadedCallbacks.push(callback);
    };


    //Creates and returns a new Coffea object, you can also pass multiple objects to create
    Coffea.create = function (t, vars) {
        if (typeof t === 'string') {
            var ele = document.createElement(t);
            for (var key in vars) {
                ele.setAttribute(key, vars[key]);
            }
            return Coffea.find(ele);
        } else if (typeof t === 'object') {
            var ret = [];
            var arg;
            for (var i = 0; i < arguments.length; i++) {
                arg = arguments[i];
                ret.push(Coffea.create(arg[0], arg[1]));
            }
            return ret;
        }
    };

    //Finds an object or collections of objects in the Coffea and adds the methods passed to the object
    Coffea.extend = function (selector, all, methods) {
        var ele;
		if(typeof selector === 'string' && (selector[0] == '#' || !all)) {
            if(typeof selector !== 'object') {
                ele = document.querySelector(selector);
                if (!ele) {
                    ele = document.getElementById(selector);
                }
            } else {
                ele = selector;
            }
            if (ele) {
                this.applyMethods(ele, methods);
                if (ele.hasOwnProperty('init') && ele.init) {
                    ele.init();
                }
            }
		}else if(typeof selector === 'object'){
			ele = selector;
			if(ele){
				this.applyMethods(ele, methods);
				if (ele.hasOwnProperty('init') && ele.init) {
					ele.init();
				}
			}
        } else {
            ele = [];
            var col = document.querySelectorAll(selector);
            if (col) {
                var i;
                for (i = 0; i < col.length; i++) {
                    this.applyMethods(col[i], methods);
                    if (col[i].hasOwnProperty('init') && col[i].init) {
                        col[i].init();
                    }
                    ele.push(col[i]);
                }
            }
        }
        return ele;
    };


    //Find a single Coffea object based on a selector and then extend it with some convenience methods
    Coffea.find = function (selector, all) {
        all = all ? true : false;
        var ele = Coffea.extend(selector, all, {
            left: null,
            top: null,
            w: null,
            h: null,
			parent:null,
            listeners: {},

            init: function () {
                this.update();
            },

            update: function () {
				try{
					this.left = this.offsetLeft;
					this.top = this.offsetTop;
				}catch(e){}
                this.w = this.offsetWidth;
                this.h = this.offsetHeight;
				this.parent = Coffea.find(this.parentNode);
            },

            css: function (props) {
                if (typeof props === 'object') {
                    var prop;
                    for (prop in props) {
                        this.style[prop] = props[prop];
                    }
                    this.update();
                    return this;
                } else {
                    return this.style[props];
                }
            },

			data: function(attr, value){
				if(!value){
					try{
					return this.getAttribute('data-' + attr);
					}catch(e){
						return null;
					}
				}else{
					this.setAttribute('data-' + attr, value);
					return this;
				}
			},

			remove_data: function(attr, value){
				this.removeAttribute('data-' + attr);
				return this;
			},

            ev: function (type, fn) {
                this.off(type, fn);

                if (window.addEventListener) {
                    this.addEventListener(type, fn, false);
                } else if (window.attachEvent) {
                    this.attachEvent('on' + type, fn);
                }

                return this;
            },

            off: function (type, fn) {
                if (window.removeEventListener) {
                    this.removeEventListener(type, fn, false);
                } else if (window.detachEvent) {
                    this.detachEvent('on' + type, fn);
                }
            },

            dispatch: function (type) {
                if (this.listeners[type]) {
                    this.listeners[type].forEach(function (el) {
                        el(this);
                    });
                }
            },

            on: function (type, fn) {
                if (!this.listeners[type]) {
                    this.listeners[type] = [];
                }
                this.listeners[type].push(fn);
                return this;
            },

            html: function (html) {
                if (arguments.length === 1) {
                    this.innerHTML = html;
                    this.dispatch('html');
                    return this;
                } else {
                    return this.innerHTML;
                }
            },

            focus: function (fn) {
                this.ev('focus', fn);
                return this;
            },

            blur: function (fn) {
				this.ev('blur', fn);
                return this;
            },

            click: function (fn) {
				this.ev('click', fn);
                return this;
            },

			change: function (fn) {
				this.ev('change', fn);
				return this;
			},

            doubleclick: function (fn) {
				this.ev('dblclick', fn);
                return this;
            },

			typing: function (fn) {
				this.ev('keyup', fn);
			},

			enter: function (fn) {
				this.ev('keyup', function inputKeyUp(e) {
                    e.which = e.which || e.keyCode;
                    if (e.which == 13) {
                        fn();
                    }
                });
                return this;
            },

            over: function (fn) {
				this.ev('mouseover', fn);
                return this;
            },

            out: function (fn) {
				this.ev('mouseleave', fn);
                return this;
            },

            destroy: function () {
                try {
                    if (this.parentNode) {
                        this.parentNode.removeChild(this);
                    }
                } catch (e) {}
                return this;
            },

            prepend: function (ele) {
                var arr = this.childNodes;
                if (Object.prototype.toString.call(ele) === '[object Array]') {
                    var frag = document.createDocumentFragment();
                    for (var i = 0; i < ele.length; i++) {
                        frag.appendChild(ele[i]);
                    }
                    this.insertBefore(frag, arr[0]);
                } else {
                    this.insertBefore(ele, arr[0]);
                }
            },

            append: function (ele) {
                if (Object.prototype.toString.call(ele) === '[object Array]') {
                    var frag = document.createDocumentFragment();
                    for (var i = 0; i < ele.length; i++) {
                        frag.appendChild(ele[i]);
                    }
                    this.appendChild(frag);
                } else {
                    this.appendChild(ele);
                }
            },

            observe: function (callback) {
                this.on('change', callback);
                Coffea.update();
            },

            bounds: function () {
                var b = {};
                var x = this.getBoundingClientRect().left;
                var y = this.getBoundingClientRect().top;
                var bot = this.getBoundingClientRect().bottom;
                var r = this.getBoundingClientRect().right;
                var h, w;
                if (this.getBoundingClientRect().width)
                    w = this.getBoundingClientRect().width;
                else
                    w = this.offsetWidth;

                if (this.getBoundingClientRect().height)
                    h = this.getBoundingClientRect().height;
                else
                    h = this.offsetHeight;
                b.x = x;
                b.y = y;
                b.h = h;
                b.w = w;
                b.b = bot;
                b.r = r;
                return b;
            },

            toggle_class: function (nm) {
                if (this.classList) {
                    this.classList.toggle(nm);
                } else {
                    //TODO: implement cross browser class toggle
                }
                return this;
            },

            has_class: function(nm){
				if(this.className){
					var m = this.className.match(new RegExp('(\\s|^)' + nm + '(\\s|$)'));
					if(m){
						return true;
					}else{
						return false;
					}
				}else{
					return false;
				}
            },

            add_class: function (nm) {
                if (this.classList) {
                    if (!this.classList.contains(nm)) {
                        this.classList.toggle(nm);
                    }
                } else {
                    if (!this.has_class(nm)) this.className += " " + nm;
                }
                return this;
            },

            remove_class: function (nm) {
                if (this.classList) {
                    this.classList.remove(nm);
                } else {
                    if(this.has_class(nm)) {
                        var reg = new RegExp('(\\s|^)' + nm + '(\\s|$)');
                        this.className = this.className.replace(reg, ' ');
                    }
                }
                return this;
            },

			preg_remove_class: function(nm){
				try{
					var reg = new RegExp('(\\s|^)' + nm + '(\\s|$)');
					this.className = this.className.replace(reg, ' ').trim();
				}catch(e){
					console.error(e);
				}
				return this;
			},

			show_inline: function(){
				this.style.display = 'inline-block';
			},

			show: function(){
				this.style.display = 'block';
			},

			hide: function(){
				this.style.display = 'none';
			},

			children: function(){
				return this.childNodes;
			},

			children_by_class: function(selector){
				return Coffea.findAll(this.getElementsByClassName(selector));
			},

			children_by_tag: function(selector){
				return Coffea.findAll(this.getElementsByTagName(selector));
			}

        });
        return ele;
    };

	var $ = window.$ = Coffea.find;

    Coffea.update = function () {
        console.log(this.collections);
    };


    //Finds a collection of Coffea objects based on a selector and then extends the collection with some convenience methods
    Coffea.findAll = function(selector){
		var ele;
		if(typeof selector === 'string'){
        	ele = Coffea.find(selector, true);
		}else{
			ele = [];
			for (var i = 0, ref = ele.length = selector.length; i < ref; i++){ele[i] = selector[i];}
		}

        //this.collections.push(selector);

        Coffea.applyMethods(ele, {
            css: function (styles) {
                if (ele.length > 0) {
                    ele.forEach(function (el, i) {
                        el.css(styles);
                    });
                }
            },

            each: function (callback) {
                if (ele.length > 0) {
                    ele.forEach(function (el, i) {
                        callback($(el), i);
                    });
                }
            },

			removeAll: function() {
				if (ele.length > 0) {
					ele.forEach(function (el, i) {
						$(el).destroy();
					});
				}
			},

			click: function(callback){
				if (ele.length > 0) {
					ele.forEach(function (el, i) {
						el.removeEventListener('click', callback, false);
						el.addEventListener('click', callback, false);
					});
				}
			},

			on: function(action, callback){
				if (ele.length > 0) {
					ele.forEach(function (el, i) {
						$(el).ev(action, callback);
					});
				}
			},

			off: function(action, callback){
				if (ele.length > 0) {
					ele.forEach(function (el, i) {
						$(el).off(action, callback);
					});
				}
			},

            first: function () {
                return ele[0];
            },

            last: function () {
                return ele[ele.length - 1];
            },

            update: function () {
				ele = Coffea.findAll(selector);
                return ele;
            }
        });
        return ele;
    };

	var agent = navigator.userAgent.toLowerCase();
	Coffea.Browser = {
		version: (agent.match( /.+(?:rv|it|ra|ie|me)[\/: ]([\d.]+)/ ) || [])[1],
		is_chrome : /chrome/.test(agent),
		is_safari : /webkit/.test(agent) && !/chrome/.test(agent),
		is_opera : /opera/.test(agent),
		is_ie : /msie/.test(agent) && !/opera/.test(agent),
		is_mozilla : /mozilla/.test(agent) && !/(compatible|webkit)/.test(agent),
		is_ipad : /iPad/i.test(agent),
		is_iphone : /iPhone/i.test(agent)
	}

	Coffea.Utils = {
		generateUUID: function(){
			var d = new Date().getTime();
			var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
				var r = (d + Math.random()*16)%16 | 0;
				d = Math.floor(d/16);
				return (c=='x' ? r : (r&0x7|0x8)).toString(16);
			});
			return uuid;
		}
	}

    //Base Event
    function E() {}
    //Expose Event
    window.Evnt = new E();

    //Return Event target
    Evnt.target = function (e) {
        e = e || window.event;
        return Coffea.find(e.target || e.srcElement);
    };


    //XHR object
    window.XHR = function () {
        this.http = new XMLHttpRequest();
        this.callback = null;
        this.method = 'GET';
        this.querystring = null;
        this.content_type = "application/x-www-form-urlencoded";
        this.upload = null;

        this.listeners = function (listeners) {
            var lxhr = Coffea.find(this.http);
            for (var l in listeners) {
                if (l == 'progress') {
					Coffea.find(this.upload).ev(l, listeners[l]);
                } else {
					lxhr.ev(l, listeners[l]);
                }
            }
		};

        this.headers = function (fields) {
            for (var key in fields) {
                this.http.setRequestHeader(key, fields[key]);
            }
            if (this.content_type !== "") {
                this.http.setRequestHeader("Content-Type", this.content_type);
            }
        };
    };

    //Call the XHR object and handle the response
    XHR.prototype.call = function (url) {
        var xhr = this;
        return new Promise(function (resolve, reject) {
            xhr.http.open(xhr.method, url);
            xhr.http.onload = function () {
                if (xhr.http.status === 200) {
                    resolve(xhr.http);
                } else {
                    reject(new Error(xhr.http.statusText));
                }
            };

            xhr.http.onerror = function () {
                reject(new Error("Network Error"));
            };

            xhr.http.send(xhr.querystring || null);
        });
    };


    //Convenience methods for calling an AJAX GET, POST, PUT and DELETE requests
    //TODO: abstract internal function to generalize the calls
    window.get = function (url, data) {
        var req = new XHR();
		return req.call(url);
    };

    window.post = function (url, data) {
        var req = new XHR();
        req.method = 'POST';
        req.querystring = data;
        return req.call(url);
    };

    window.put = function (url, data) {
        var req = new XHR();
        req.method = 'PUT';
        req.querystring = data;
        return req.call(url);
    };

    window.del = function (url, data) {
        var req = new XHR();
        req.method = 'DELETE';
        req.querystring = data;
        return req.call(url);
    };


	//Base Model

    var M = function(){
        this.name = null;

        this.init = function(){};

        this.extend = function(a) {
            var prop;
            for (prop in this) {
                if (a.hasOwnProperty(prop) && this.hasOwnProperty(prop) === false) {
                    this[prop] = a[prop];
                }
            }
            return this;
        };
    };

    window.Model = new M();

    var A = function(){};

    window.App = new A();

    //Bind the Coffea load and onLoad functions
    var readyState = document.readyState;
    document.addEventListener("DOMContentLoaded", CoffeaLoaded, false);
    window.addEventListener("load", pageLoaded, false);

	var toggle;
	var checkSize = function(){
		var m = window.matchMedia("(max-width: 1024px)");
		if(m.matches){
			var body = Coffea.find('wrapper');
			if(!toggle){
				toggle = Coffea.create('span', {title:"list", id:"toggle", "class": 'fa fa-navicon'});
				var nav = Coffea.find('nav');
				var wrapper = Coffea.find('wrapper');
				toggle.click(function(){
					nav.toggle_class('open');
					wrapper.toggle_class('open');
					toggle.toggle_class('open');
				});
				body.appendChild(toggle);
			}
		}else{
			if(toggle){
				toggle.destroy();
				toggle = null;
			}
		}
	}

	var overrideLinks = function(){
		var links = Coffea.findAll('a');
		links.click(function(e){
			e.stopPropagation();
			e.preventDefault();
			var target = Evnt.target(e);
			window.history.pushState("", "", target.href);
			return false;
		});
	}

	var defaultLoad = function(){
		checkSize();
		overrideLinks();
	}

	var pushState = window.history.pushState;
	window.history.pushState = function(state) {
		var ret = pushState.apply(window.history, arguments);
		if(typeof window.history.onpushstate == "function") {
			window.history.onpushstate({state: state});
		}
		return ret;
	}

	window.addEventListener("resize", checkSize, false);
	window.addEventListener("load", defaultLoad, false);


	//Dialog object

	window.openDialog = null;

	window.Dialog =  function(type, msg, ele, append, callback){
		this.init = function(type){
			return this[type]();
		}

		this.tooltip = function(){
			var tooltip = Coffea.create('div', {'class':'tooltip'});
			tooltip.innerHTML = this.msg;
			if(this.append)
				tooltip.append(this.append);
			var bounds = this.ele.bounds();
			var body = document.querySelector('body');
			body.appendChild(tooltip);
			tooltip.css({
				left:bounds.r + 25 + 'px',
				top: (bounds.b + 25) - (tooltip.bounds().h/2) + 'px',
			});
			return tooltip;
		}

		this.modal = function(){
			var modalBackground = Coffea.create('div', {'class':'modal-bg'});
			var modal = Coffea.create('div', {'class':'modal-open'});
			modal.innerHTML = this.msg;
			var body = document.querySelector('body');
			modalBackground.appendChild(modal);
			body.appendChild(modalBackground);
			modal.add_class('open');
			return modalBackground;
		}

		this.dialog = function(){
			var modal = Coffea.create('div', {'class':'blklab-modal'});
			modal.innerHTML = this.msg;
			var body = document.querySelector('body');
			body.appendChild(modal);
			modal.add_class('open');
			$('blklab-submit').click(this.submit);
			$('blklab-cancel').click(function(){
				modal.destroy();
			});
			return modal;
		}

		this.submit = callback;

		this.ele = ele;
		this.msg = msg;
		this.append = append;
		return this.init(type, ele);
	};

	window.FilePicker = function(data, callback){
		this.heights = []
		this.images = null;

		this.init = function(){
			var body = Coffea.find('#body');
			var background = Coffea.create('div', {'lb-picker':'', 'class':'fade'}, this.data);
			body.appendChild(background);
			this.images = Coffea.findAll('img');
			var scope = this;


			this.run(background);

			Coffea.find(window).ev('resize', function(){
				scope.run(background);
			});

			Coffea.extend('[lb-image-search]', true, {
				oblur: function(ev){
					var inp = this;
					var value = inp.value.toLowerCase();

					if(value != '' && value.length > 3){
						scope.images.each(function(el){
							if(el.getAttribute('src').toLowerCase().indexOf(value) != -1 || el.getAttribute('data-caption').toLowerCase().indexOf(value) != -1){
								el.parentNode.style.display = 'inline-block';
							}else{
								el.parentNode.style.display = 'none';
							}
						});
					}else{
						scope.images.each(function(el){
							el.parentNode.style.display = 'inline-block';
						});
					}
					scope.run(background);
				}.on('blur')
			});
		}

		this.run = function(background){
			var max_height = 300;
			var size = background.querySelector('.container').offsetWidth;//window.innerWidth;
			var n = 0;
			var images = this.images;
			images.each(function(el){
				el.click(function(){
					background.destroy();
					callback.bind(el).call(null, el.src);
				});
			});
			var scope = this;
			w: while(images.length > 0){
				for(var i = 1; i <= images.length; ++i){
					var slice = images.slice(0 , i);
					var h = scope.getHeight(slice, size);
					if(h < max_height){
						scope.setHeight(slice, Math.min(max_height, h));
						images = images.slice(i);
						continue w;
					}
				}
				this.setHeight(slice, Math.min(max_height, h));
				break;
			}
		}

		this.getHeight = function(images, width){
			width -= images.length * 5;
			var h = 0;
			var img;
			for (var i = 0; i < images.length; ++i) {
				img = images[i];
				h += img.getAttribute('data-width') / img.getAttribute('data-height');
			}
			return width / h;
		}

		this.setHeight = function(images, height){
			this.heights.push(height);
			var img;
			for (var i = 0; i < images.length; ++i) {
				img = images[i];
				//img.style.width = (height * img.getAttribute('data-width') / img.getAttribute('data-height')) + 'px';
				img.style.height = height + 'px';
			}
		}

		this.data = data;
		this.callback = callback;
		this.init();
	}

	window.Gallery =  function(ele, type, len, delay, imgs){
		this.init = function(ele, type){
			this[type](ele);
		}

		this.crossfade = function(ele){
			var slider = ele.querySelector('.slider');
			console.log(this.imgs);
			/*var scope = this;
    		this.i = 0;
    		var current_cell = arr[0];
    		var next_cell = arr[this.i+1];

	    	var run = function(){
    			window.clearInterval(timer);
    			scope.i = scope.i < scope.len ? scope.i + 1 : 0;
    			timer = window.setInterval(run, scope.delay);
    		};

	    	current_cell.add_class('fade');
	    	timer = window.setInterval(run, this.delay);	*/
		};

		this.slide = function(ele){
			var slider = Coffea.find(ele.children_by_class('slider')[0]);
			var cells = slider.childNodes;
			var slider_bounds = slider.bounds();
			var animate, end, start, timer;
			var scope = this;
			var run = function(){
				window.clearInterval(timer);
				start = slider.offsetLeft;
				end = scope.i == 0 ? 0 : -1 * (scope.bounds.w * scope.i);
				animate = new animation(300);
				animate.run(slider, 'left', start, end, function(){
					scope.i = scope.i < scope.len ? scope.i + 1 : 0;
					slider_bounds = slider.bounds();
					timer = window.setInterval(run, scope.delay);
				});
			};

			timer = window.setInterval(run, this.delay);

			Coffea.find(window).ev('resize', function(){
				window.clearInterval(timer);
				scope.bounds = ele.bounds();
				for(var i=0; i<cells.length; i++){
					cells[i].css({
						width: scope.bounds.w + 'px'
					});
				}
				slider.css({
					left: '0px'
				});
				scope.i = 1;
				timer = window.setInterval(run, scope.delay);
			});
		};

		this.scroll = function(ele){
			console.log('scroll');
		};

		this.bounds = ele.bounds();
		this.i = 1;
		this.len = len-1;
		this.delay = delay || 5000;
		this.imgs = imgs;
		this.init(ele, type);
	};

	function animation(duration, type){
		this.elem = null;
		this.startTime = undefined;
		this.time = undefined;
		this.duration = duration || 500;
		this.type = type || null;
		this.target = 0;
		this.start = 0;
		this.prop = null;
		this.callback = null;

		this.swing = function(p, n, firstNum, diff){
			return ((-Math.cos(p * Math.PI) / 2) + 0.5) * diff + firstNum;
		}

		this.loop = function(){
			this.time = Date.now();
			if(this.startTime === undefined)
				this.startTime = this.time;

			if(this.started){
				var dur = this.duration + this.startTime;
				if(this.time >= dur){
					this.started = false;
					this.time = undefined;
					this.startTime = undefined;
					if(this.type == 'window'){
						window.scrollTo(0, this.target);
					}else{
						this.elem.style[this.prop] = this.target + 'px'
					}

					if(this.callback)
						this.callback();
				}else{
					var curTime = this.time - this.startTime;
					var curPos = curTime / this.duration;
					var diff = (this.target - this.start);
					var ease = this.swing(curPos, curTime, 0, 1, this.duration);
					var val = Math.ceil(this.start + ((this.target - this.start) * ease));
					//console.log(val)
					if(this.type == 'window'){
						window.scrollTo(0, val);
					}else{
						this.elem.style[this.prop] = val + 'px'
					}
				}
			}
		}

		this.run = function(elem, prop, start, end, callback){
			var that = this;
			this.elem = elem;
			this.start = start;
			this.target = end;
			this.prop = prop;
			this.callback = callback;
			this.started = true;
			(function animloop(){
				window.requestAnimationFrame(animloop);
				that.loop();
			})();
		}
	}

}(window, document));
