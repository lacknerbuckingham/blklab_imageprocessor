(function(history){
	BlkLab.Router = function(){
		var self = this;
		this.path = '';
		this.def = '/';
		this._routes = {default:''}
		this.debug = true;

		this.init = function(){
			window.onpopstate = history.onpushstate = function(e){
				if(BlkLab.History.type == 'push')
					BlkLab.App.Router.dispatch(location.pathname);
			}

			window.onhashchange = function(e){
				if(BlkLab.History.type == 'hash')
					BlkLab.App.Router.dispatch(location.hash.replace('#', ''));
			}
		}

		this.init();
	}

	BlkLab.Router.prototype = {
		routes: function(routes){
			for(var key in routes){
				this.route(key, routes[key]);
			}
		},

		route: function(path, methods){
			this._routes[path] = methods;
		},

		dispatch: function(path){
			path = path || location.pathname;
			if(this._routes[path]){
				this.run(path);
			}else if(this._routes.default){
				this.run(path);
			}else{
				this.notFound();
			}
		},

		run: function(path){
			this.path = path;
			var route = this._routes[path] || this._routes.default;
			route.controller.render.call(this, path);
			/*route.model().then(function(model){
				route.model = model;
			}).then(function(){

			}).then(this.allDone, this.fail);*/
		},

		notFound: function(){
			console.log('404');
		},

		allDone: function(){
			if(this.debug)
				console.log('rendered successfully');
		},

		fail: function(){
			if(this.debug)
				console.log('failed');
		}
	}
})(window.history);
