(function (window, document) {
	'use strict';

	//XHR object
	window.XHR = function () {
		this.http = new XMLHttpRequest();
		this.callback = null;
		this.method = 'GET';
		this.querystring = null;
		this.content_type = "application/x-www-form-urlencoded";
		this.upload = null;

		this.listeners = function (listeners) {
			var lxhr = Coffea.find(this.http);
			for (var l in listeners) {
				if (l == 'progress') {
					Coffea.find(this.upload).ev(l, listeners[l]);
				} else {
					lxhr.ev(l, listeners[l]);
				}
			}
		};

		this.headers = function (fields) {
			for (var key in fields) {
				this.http.setRequestHeader(key, fields[key]);
			}
			if (this.content_type !== "") {
				this.http.setRequestHeader("Content-Type", this.content_type);
			}
		};
	};

	//Call the XHR object and handle the response
	XHR.prototype.call = function (url) {
		var xhr = this;
		return new Promise(function (resolve, reject) {
			xhr.http.open(xhr.method, url);
			xhr.http.onload = function () {
				if (xhr.http.status === 200) {
					resolve(xhr.http);
				} else {
					reject(new Error(xhr.http.statusText));
				}
			};

			xhr.http.onerror = function () {
				reject(new Error("Network Error"));
			};

			xhr.http.send(xhr.querystring || null);
		});
	};


	//Convenience methods for calling an AJAX GET, POST, PUT and DELETE requests
	//TODO: abstract internal function to generalize the calls
	window.get = function (url, data, success, err) {
		success = arguments.length < 4 ? data : success;
		err = arguments.length < 4 ? success : err;
		var req = new XHR();
		return req.call(url).then(success, err);
	};

	window.post = function (url, data, success, err) {
		success = arguments.length < 4 ? data : success;
		err = arguments.length < 4 ? success : err;
		var req = new XHR();
		req.method = 'POST';
		req.querystring = data;
		return req.call(url).then(success, err);
	};

	window.put = function (url, data, success, err) {
		success = arguments.length < 4 ? data : success;
		err = arguments.length < 4 ? success : err;
		var req = new XHR();
		req.method = 'PUT';
		req.querystring = data;
		return req.call(url).then(success, err);
	};

	window.del = function (url, data, success, err) {
		success = arguments.length < 4 ? data : success;
		err = arguments.length < 4 ? success : err;
		var req = new XHR();
		req.method = 'DELETE';
		req.querystring = data;
		return req.call(url).then(success, err);
	};

	BlkLab.History = {
		type: 'push',
		base: '',

		start: function(type){
			this.base = location.protocol + '//' + location.hostname;
			if(type == 'hash'){
				this.type = "hash";
			}else{
				this.type = "push";
				var pushState = window.history.pushState;
				window.history.pushState = function(state) {
					var ret = pushState.apply(window.history, arguments);
					if(typeof window.history.onpushstate == "function") {
						window.history.onpushstate({state: state});
					}
					return ret;
				}
			}

			document.documentElement.addEventListener("click", this.intercept, false);
		},

		intercept: function(e){
			var target = Evnt.target(e);
			if(target.tagName == 'A'){
				e.stopPropagation();
				e.preventDefault();
				var href = target.href.replace(BlkLab.History.base, '');
				if(BlkLab.History.type == 'push'){
					window.history.pushState("", "", href);
				}else if(BlkLab.History.type == 'hash'){
					location.hash = href;
				}
				return false;
			}
		}
	}


	//Base Event
	function E() {}
	//Expose Event
	window.Evnt = new E();

	//Return Event target
	Evnt.target = function (e) {
		e = e || window.event;
		return e.target || e.srcElement;
	};

}(window, document));
