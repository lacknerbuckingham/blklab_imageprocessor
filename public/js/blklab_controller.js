BlkLab.Controller = function(){}

BlkLab.Controller.prototype = {
	render: function(){
		console.log('base');
	}
}

BlkLab.Controller.extend = function(methods){
	var self =  function(){
		BlkLab.Controller.call(this);
	}

	self.prototype = Object.create(BlkLab.Controller.prototype);
	self.prototype.constructor = self;

	for(method in methods){
		if(self.prototype.hasOwnProperty(method) === false) {
			self.prototype[method] = methods[method];
		}
	}
	var obj = new self();
	return obj;
}
