(function(){
	'use strict';

	window.Files = {
		readFile: function(f, callback){
			var reader = new FileReader();
			reader.onload = callback;
			if(typeof f === 'object'){
				reader.readAsDataURL(f);
			}
		},

		handle: function(files, scope){
			var len = files.length;
			if(len == 1){
				var f = files[0];
				scope.file = f;
				Files.readFile(f, function(e) {
					console.log('loading');
					var img = Coffea.create('img', {});
					img.src = e.target.result;

					img.onload = function(){
						var height = img.height;
						var width = img.width;
						if(width>height){
							img.add_class('wide-img');
						}else{
							img.add_class('tall-img');
						}
						scope.innerHTML = '';
						scope.append(img);
					}
				});
			}else{
				if(scope.tagName.toLowerCase() !== 'img'){
					scope.css({'backgroundImage': ''})
				}else{
					scope.setAttribute('src', '')
					var newscope = Coffea.create('section', {'lb-tag': scope.getAttribute('lb-tag'), 'lb-image':'', id: scope.id});
					scope.parentNode.replaceChild(newscope, scope);
					scope = newscope;
				};
				scope.add_class('gallery');
				scope.setAttribute('lb-gallery', '');
				var bounds = scope.bounds();
				var slider = Coffea.create('section', {'class': 'slider'});
				var i;
				var imgs = [];
				for(i=0; i<len; i++){
					this.file = files[i];
					Files.readFile(files[i], function(e) {
						var cell, img, src;
						src = e.target.result;
						cell = Coffea.create('div', {'class':'cell'});
						img = Coffea.create('img', {'src':src});
						imgs.push(img);
						cell.css({
							width:bounds.w + 'px'
						});
						cell.appendChild(img);
						slider.appendChild(cell);
					});
				};
				scope.appendChild(slider);
				console.log(imgs);
				var gallery = new Gallery(scope, 'slide', len, 5000, imgs);
			}
		}
	}

	window.Ranger = {
		selection: null,
		range: null,
		parentElements: ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'blockquote', 'pre'],

		save: function(containerEl) {
			try{
				var doc = containerEl.ownerDocument, win = doc.defaultView;
				var range = win.getSelection().getRangeAt(0);
				var preSelectionRange = range.cloneRange();
				preSelectionRange.selectNodeContents(containerEl);
				preSelectionRange.setEnd(range.startContainer, range.startOffset);
				var start = preSelectionRange.toString().length;

				var i,
					len,
					ranges,
					sel = window.getSelection();
				if(sel.getRangeAt && sel.rangeCount) {
					ranges = [];
					for (i = 0, len = sel.rangeCount; i < len; i += 1) {
						ranges.push(sel.getRangeAt(i));
					}
				}

				this.selection = {
					start: start,
					end: start + range.toString().length,
					text: range.toString(),
					anchor: win.getSelection().anchorNode,
					ranges: ranges
				}

			}catch(e){
				this.selection = {
					start: 0,
					end: 0,
					text: null,
					anchor: null,
					ranges: null
				}
			}
		},

		restore: function(containerEl, type) {
			if(type == 'block'){
				var doc = containerEl.ownerDocument, win = doc.defaultView;
				var charIndex = 0, range = doc.createRange();
				range.setStart(containerEl, 0);
				range.collapse(true);
				var nodeStack = [containerEl], node, foundStart = false, stop = false;

				while (!stop && (node = nodeStack.pop())) {
					if (node.nodeType == 3) {
						var nextCharIndex = charIndex + node.length;
						if (!foundStart && this.selection.start >= charIndex && this.selection.start <= nextCharIndex) {
							range.setStart(node, this.selection.start - charIndex);
							foundStart = true;
						}
						if (foundStart && this.selection.end >= charIndex && this.selection.end <= nextCharIndex) {
							range.setEnd(node, this.selection.end - charIndex);
							stop = true;
						}
						charIndex = nextCharIndex;
					} else {
						var i = node.childNodes.length;
						while (i--) {
							nodeStack.push(node.childNodes[i]);
						}
					}
				}
				var sel = win.getSelection();
				sel.removeAllRanges();
				sel.addRange(range);
			}else{
				var i,
					len,
					sel = window.getSelection();
				if(this.selection && this.selection.ranges){
					sel.removeAllRanges();
					for (i = 0, len = this.selection.ranges.length; i < len; i += 1) {
						sel.addRange(this.selection.ranges[i]);
					}
				}
			}

			return sel;
		},

		getSelectionStart: function (){
			var node = document.getSelection().anchorNode,
				startNode = (node && node.nodeType === 3 ? node.parentNode : node);
			return startNode;
		},

		getSelection: function() {
			return window.getSelection();
		},

		getSelectionStartNode: function() {
			var node, selection;
			if (window.getSelection) {
				// FF3.6, Safari4, Chrome5 (DOM Standards)
				selection = getSelection();
				node = selection.anchorNode;
			} else if (!node && document.selection) {
				// IE
				selection = document.selection
				var range = selection.getRangeAt ? selection.getRangeAt(0) : selection.createRange();
				node = range.commonAncestorContainer ? range.commonAncestorContainer : range.parentElement ? range.parentElement() : range.item(0);
			}

			if (node != undefined) {
				var n = (node && node.nodeType === 3 ? node.parentNode : node);
				return n;
			}
		},

		getSelectionHtml: function(){
			var i,
				html = '',
				sel,
				len,
				container;
			if (window.getSelection !== undefined) {
				sel = window.getSelection();
				if (sel.rangeCount) {
					container = document.createElement('div');
					for (i = 0, len = sel.rangeCount; i < len; i += 1) {
						container.appendChild(sel.getRangeAt(i).cloneContents());
					}
					html = container.innerHTML;
				}
			} else if (document.selection !== undefined) {
				if (document.selection.type === 'Text') {
					html = document.selection.createRange().htmlText;
				}
			}
			return html;
		},

		getSelectionNode: function(){
			var tagName;
			var el = this.selection.anchor;

			if (el && el.tagName) {
				tagName = el.tagName.toLowerCase();
			}
			if(el){
				while (el && this.parentElements.indexOf(tagName) === -1) {
					el = el.parentNode;
					if (el && el.tagName) {
						tagName = el.tagName.toLowerCase();
					}
				}

				return {
					el: el,
					tagName: tagName
				};
			}else{
				return {
					el: el,
					tagName: 'p'
				};
			}
		}
	};

	var body = document.querySelector('body');
	var win = $(window);
	var doc = $(document.documentElement);

	window.Editor = function(){
		var _self = this;
		this.menuOpen = false;
		this.toolbar;
		this.layoutBtn;
		this.metaBtn;
		this.editmenu;
		this.content;
		this.body;
		this.buttons = {};
		this.blocks;
		this.insertionPoints;
		this.editbar;
		this.timer;
		this.editBlock;
		this.drag = 0;
		this.startDrag;
		this.linkUpdating = false;
		this.link;
		this.linkDialog;
		this.sort;
		this.htmlBlocks = ['h1','h2','h3','p', 'blockquote'];
		this.textStyles = {'bold':'b', 'italic':'i'};
		this.textAlignments = ['justifyLeft', 'justifyRight', 'justifyCenter'];
		this.currentTag;
		this.editButtons;
		this.point;

		this.init = function(){
			get('/api/templates/toolbar', function(http){
				var toolbar = Coffea.create('section', {id:"blklab-toolbar"});
				toolbar.innerHTML = http.responseText;
				body.appendChild(toolbar);
				_self.load();
			},function(err){
				console.log(err);
			});
		}

		this.update = function(){
			this.enableAllTextblocks();
		}

		this.load = function(){
			//document.execCommand("enableObjectResizing", true, null);
			document.execCommand("styleWithCSS", null, false);

			this.enableAllTextblocks();

			this.toolbar = $('#blklab-toolbar');
			this.editmenu = $('#menu');
			this.content = $('#content');
			this.drag = Coffea.findAll('[draggable]');
			this.body = document.querySelector('body');
			this.editbar = $('editbar');

			this.sort = new Layout('blklab-block', 'content', _self, _self.update);

			this.buttons['layout'] = $('.layoutBtn');
			this.buttons['meta'] = $('.metaBtn');
			this.buttons['config'] = $('.configBtn');
			this.buttons['stack'] = $('.stackBtn');
			this.buttons['save'] = $('.saveBtn');


			this.buttons.layout.click(this.toggleMenu);
			this.buttons.meta.click(this.openModal);
			this.buttons.config.click(this.openModal);
			this.buttons.stack.click(this.openModal);
			this.buttons.save.click(this.savePage);

			var editLayout = $('.editLayoutBtn');
			editLayout.click(function(){
				_self.sort.toggle();
				editLayout.toggle_class('on');
			})

			var linkDialog = this.linkDialog = $('link_dialog');
			var link = this.link = $('link_address');
			var linkAdd = $('link_add');

			var addLink = function(){
				var href = link.value;
				if(!_self.linkUpdating){
					var selection = Ranger.restore(_self.editBlock);
					var node = Ranger.getSelectionNode();
					var link_text = Ranger.selection.text;
					var html = '<a href="' + href + '">' + link_text + '</a>';
					document.execCommand('insertHTML', false, html);
				}else{
					if(link.value != ''){
						Ranger.selection.anchor.parentElement.href = link.value;
					}else{
						var node = document.createTextNode(Ranger.selection.anchor.data);
						Ranger.selection.anchor.parentElement.parentElement.replaceChild(node, Ranger.selection.anchor.parentElement);
					}
				}

				linkDialog.toggle_class('on');
				_self.editbar.toggle_class('expanded');
				_self.hideToolbar();
				link.value = '';
				_self.linkUpdating = false;
			}

			doc.ev('keydown', function(ev){
				if(ev.which === 13){
					if(Evnt.target(ev) == link){
						ev.preventDefault();
						addLink();
					}
				}
			});

			linkAdd.click(addLink);

			this.editButtons = Coffea.findAll('.blklab-editbar-button');

			this.editButtons.click(function(e){
				window.clearInterval(_self.timer);
				_self.stopping = false;
				e.preventDefault();
				e.stopPropagation();
				var ele = Evnt.target(e);
				var action = ele.data('action');
				switch(action){
					case 'format':
						var type = ele.data('type');
						if(_self.htmlBlocks.indexOf(type) != -1){
							type = type == _self.currentTag.tagName.toLowerCase().trim() ? 'p' : type;
							Ranger.restore(_self.editBlock, 'block');
							document.execCommand("formatBlock", false, type);
						}else{
							Ranger.restore(_self.editBlock);
							if(type.indexOf('justify') == -1){
								document.execCommand(type, false, true);
								if(type == 'insertUnorderedList' && _self.currentTag.tagName.toLowerCase().trim() == 'li'){
									document.execCommand("formatBlock", false, 'p');
								}
							}else{
								var i;
								for(i=0;i<_self.textAlignments.length;i++){
									_self.currentTag.remove_class(_self.textAlignments[i]);
								}
								_self.currentTag.add_class(type);
							}
						}

						Ranger.save(_self.editBlock);
						_self.currentTag = Ranger.getSelectionStartNode();
						_self.activateButtons();
						if(_self.currentTag.tagName.toLowerCase() == 'span'){
							_self.currentTag.parentNode.innerHTML = _self.cleanHTML(_self.currentTag.parentNode.innerHTML, 'class');
							_self.currentTag.parentNode.replaceChild(document.createTextNode(_self.currentTag.innerText), _self.currentTag);
						}

						break;
					case 'link_dialog':
						linkDialog.toggle_class('on');
						_self.editbar.toggle_class('expanded');
						link.focus(function(){
							window.clearInterval(_self.timer);
							_self.stopping = false;
						});
						break;
				}

			});

			doc.doubleclick(function(ev){
				var tmp = Ranger.selection;
				Ranger.save(Evnt.target(ev));
				/*if(Ranger.getSelectionStartNode().href){
					window.clearInterval(_self.timer);
					_self.linkUpdating = true;
					_self.showToolbar();
					linkDialog.add_class('on');
					_self.editbar.add_class('expanded');
					link.value = Ranger.getSelectionStartNode().href;
					link.focus(function(){
						window.clearInterval(_self.timer);
						_self.stopping = false;
					});
				}else if(Ranger.getSelectionStartNode().tagName.toLowerCase() === 'img'){
					console.log('test');
				}else{
				}*/

				var parent = Evnt.target(ev);
				while(parent && !parent.has_class('blklab-text')){
					parent = parent.parent;
				}

				if(Ranger.selection.text != '' && parent){
					_self.link.value = '';
					_self.linkDialog.remove_class('on');
					_self.editbar.remove_class('expanded');
					_self.showToolbar();
				}
			});

			doc.ev('mousedown', function(){
				_self.startDrag = null;
				_self.drag = 0;
			});

			doc.ev('mouseup', function(){
				if(_self.startDrag && _self.drag){
					window.clearInterval(_self.timer);
					_self.stopping = false;
					_self.link.value = '';
					_self.linkDialog.remove_class('on');
					_self.editbar.remove_class('expanded');
					_self.showToolbar();
				}
			});

			Coffea.findAll('.block-btn').click(_self.addBlock);

			win.click(function(ev){
				var target = Evnt.target(ev);
				if(_self.menuOpen && !target.has_class('layoutBtn')){
					_self.toggleMenu(ev);
				}
			});


		}

		this.savePage = function(){
			var d = []
			Coffea.findAll('.blklab-block').each(function(el){
				var ele = $(el);
				var data = {
					'type': ele.data('type'),
					'html': ele.innerHTML
				}
				d.push(data);
			});

			var insertionPoints = Coffea.findAll('.blklab-insertion-point');
			insertionPoints.removeAll();

			if(_self.sort.active)
				_self.sort.unload();
			localStorage.setItem(location.href, $('content').innerHTML);
		}

		//Menus

		this.toggleMenu = function(ev){
			ev.preventDefault();
			ev.stopPropagation();
			var target = Evnt.target(ev);

			var toggle = function(){
				_self.editmenu.toggle_class('show');
				_self.toolbar.toggle_class('on');
				_self.buttons.layout.toggle_class('on');
				_self.menuOpen = !_self.menuOpen;
			}

			if((target.id != 'menu' && !target.parent.has_class('block-btn') && target.parentNode.id != 'menu')){
				if(target.has_class('blklab-insertion-point')){
					_self.point = target;
				}
				toggle();
			}
		}

		this.openModal = function(ev){
			var target = Evnt.target(ev);
			var type = target.data('type');
			_self.buttons[type].toggle_class('on');
			_self.toolbar.toggle_class('on');

			get('/api/templates/' + type, function(http){
				var closeModal = function(){
					modal.remove_class('open');
					_self.buttons[type].remove_class('on');
					_self.toolbar.remove_class('on');
					var timer = setInterval(function(){
						clearInterval(timer);
						timer = null;
						modal.destroy();
					},300);
				}

				var modal = new Dialog('modal', http.responseText);
				var close = $('close-meta');
				modal.toggle_class('open');
				$('.cancel').click(closeModal);
				close.click(closeModal);
				modal.appendChild(close);

				if(_self[type + 'Modal']){
					_self[type + 'Modal'].call(_self);
				}
			},function(err){
				console.log(err);
			});
		}

		this.metaModal = function(){
			var nav_title = $('#nav_title');
			var title = $('#title');
			var ident = $('#identifier');

			nav_title.typing(function(){
				title.value = nav_title.value;
				ident.value = nav_title.value.toLowerCase().trim().replace(/ /g,'-').replace(/[-]+/g, '-').replace(/[^\w-]+/g,'');
			});
		}

		this.openConfig = function(){}

		//Blocks

		this.addBlock = function(ev){
			var target = Evnt.target(ev).parentNode;
			var blockType = target.data('type') + 'Block';

			if(_self[blockType])
				_self[blockType].bind(_self).call(_self);

			return _self;
		}

		this.imageBlock = function(){
			var block = Coffea.create('section', {'class':'blklab-block blklab-image'});
			block.data('type', 'image');
			var addImgButton = Coffea.create('div', {'class':'fa fa-picture-o blklab-new-image'});
			var addImgInput = Coffea.create('input', {'type':'file', name:'blklab-images'});
			addImgButton.append(addImgInput);
			block.append(addImgButton);
			if(_self.point){
				_self.content.insertBefore(block, _self.point);
				_self.point = null;
			}else{
				_self.content.appendChild(block);
			}

			addImgInput.change(function(ev){
				var scope = this;
				var files = ev.target.files;
				if(files.length > 0){
					Files.handle(files, block);
				}
			});

			block.ev('dragover', function(ev){
				ev.stopPropagation();
				ev.preventDefault();
				ev.dataTransfer.dropEffect = 'copy';
				this.remove_class('fade');
				this.add_class('entered');
			});

			block.ev('dragenter', function(ev){
				ev.stopPropagation();
				ev.preventDefault();
			});

			block.ev('dragleave', function(ev){
				ev.stopPropagation();
				ev.preventDefault();
				this.remove_class('entered');
			});

			block.ev('drop', function(ev){
				ev.stopPropagation();
				ev.preventDefault();
				this.remove_class('entered');
				var scope = this;
				var files = ev.dataTransfer.files;
				if(files.length > 0){
					Files.handle(files, scope);
				}
			});

			_self.sort.update();
		}

		this.galleryBlock = function(){
			var block = Coffea.create('section', {'class':'blklab-block blklab-gallery'});
			block.data('type', 'gallery');
			var addImgButton = Coffea.create('div', {'title':'picture', 'class':'entypo blklab-new-gallery'});
			addImgButton.innerHTML = '🌄';
			var addImgInput = Coffea.create('input', {'type':'file', name:'blklab-images'});
			addImgInput.setAttribute('multiple', true);
			addImgButton.append(addImgInput);
			block.append(addImgButton);
			_self.content.appendChild(block);

			addImgInput.change(function(ev){
				var scope = this;
				var files = ev.target.files;
				if(files.length > 0){
					Files.handle(files, block);
				}
			});

			block.ev('dragover', function(ev){
				ev.stopPropagation();
				ev.preventDefault();
				ev.dataTransfer.dropEffect = 'copy';
				this.remove_class('fade');
				this.add_class('entered');
			});

			block.ev('dragenter', function(ev){
				ev.stopPropagation();
				ev.preventDefault();
			});

			block.ev('dragleave', function(ev){
				ev.stopPropagation();
				ev.preventDefault();
				this.remove_class('entered');
			});

			block.ev('drop', function(ev){
				ev.stopPropagation();
				ev.preventDefault();
				this.remove_class('entered');
				var scope = this;
				var files = ev.dataTransfer.files;
				if(files.length > 0){
					Files.handle(files, scope);
				}
			});

			_self.sort.update();
		}

		this.videoBlock = function(ev){
			var pattern1 = /(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(.+)/g;
			var pattern2 = /(?:http?s?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/g;
			var pattern3 = /([-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?(?:jpg|jpeg|gif|png))/gi;

			var html = '<h2><span class="fa fa-film"></span> Add Video</h2><div id="link_dialog"><input type="text" id="blklab-address" placeholder="Paste or type a link"><div class="clear"></div><input type="button" id="blklab-submit" value="Add"> <input type="button" id="blklab-cancel" value="Cancel"></div>';

			var modal = new Dialog('dialog', html, Evnt.target(ev), null, function(){
				var link = $('#blklab-address').value;
				var type = '';
				if(pattern1.test(link)){
					console.log('vimeo');
					var replacement = '<iframe src="//player.vimeo.com/video/$1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
					var html = link.replace(pattern1, replacement);
					type = 'vimeo';
				}


				if(pattern2.test(link)){
					console.log('youtube');
					var replacement = '<iframe src="http://www.youtube.com/embed/$1" frameborder="0" allowfullscreen></iframe>';
					var html = link.replace(pattern2, replacement);
					type = 'youtube';
				}


				if(pattern3.test(link)){
					console.log('img');
					var replacement = '<a href="$1" target="_blank"><img class="sml" src="$1" /></a><br />';
					var html = link.replace(pattern3, replacement);
					type = 'img';
				}

				if(type){
					var block = Coffea.create('section', {'class':'blklab-block blklab-video widescreen ' + type});
					block.data('type', 'video');
					block.innerHTML = html;
					if(_self.point){
						_self.content.insertBefore(block, _self.point);
						_self.point = null;
					}else{
						_self.content.appendChild(block);
					}
					modal.destroy();
				}else{
					alert('invalid video url');
				}
			});

			$('#blklab-address').value = 'http://vimeo.com/9214773';

			_self.sort.update();
		}

		this.map_cnt = 0;
		this.maps = [];
		this.mapBlock = function(){
			_self.map_cnt++;
			var block = Coffea.create('section', {'class':'blklab-block blklab-map', 'id':'blklab-map-' + _self.map_cnt});
			block.data('type', 'map');
			if(_self.point){
				_self.content.insertBefore(block, _self.point);
				_self.point = null;
			}else{
				_self.content.appendChild(block);
			}

			var show_map = function(position){
				var mapOptions = {
					zoom: 8,
					center: new google.maps.LatLng(position.coords.latitude, position.coords.longitude)
				};
				var map = new google.maps.Map(document.getElementById('blklab-map-' + _self.map_cnt), mapOptions);
				_self.maps.push(map);
			}

			if(navigator.geolocation){
				navigator.geolocation.getCurrentPosition(show_map);
			}else{
				var coords = {coords: {latitude: -34.397, longitude: 150.644}}
				show_map(coords);
			}

			_self.sort.update();
		}

		this.textBlock = function(){
			var block = Coffea.create('section', {'class':'blklab-block blklab-text placeheld'});
			var placeholder = '<span>Write Here...</span>';
			block.data('type', 'text');
			block.innerHTML = placeholder;
			block.contentEditable = true;
			if(_self.point){
				_self.content.insertBefore(block, _self.point);
				_self.point = null;
			}else{
				_self.content.appendChild(block);
			}

			_self.enableTextBlock(block);
			_self.sort.update();
		}

		this.enableAllTextblocks = function(){
			var textBlocks = Coffea.findAll('.blklab-text');
			textBlocks.each(function(block){
				_self.enableTextBlock(block);
			});
		}

		this.enableTextBlock = function(block){
			var placeholder = '<span>Write Here...</span>';
			block.ev('mousedown', function(){
				_self.startDrag = null;
				_self.drag = 0;
			});

			block.ev('mousemove', function(){
				_self.startDrag = block;
				_self.drag = 1;
			});

			block.ev('keydown', function(ev){
				var node = Ranger.getSelectionStart();

				if(block.innerHTML == placeholder){
					block.toggle_class('placeheld');
					block.innerHTML = '';
				}

				if(ev.which === 13){
					if(node.tagName != 'li'){
						if(!ev.shiftKey){
							//ev.preventDefault();
						}
					}
				}else{
					Ranger.save(block);
					if(ev.which >= 37 && ev.which <= 40){
						if(Ranger.selection.text != '' && block.innerHTML != ''){
							_self.showToolbar();
						}else{
							_self.hideToolbar();
						}
					}
				}
			});

			block.ev('keyup', function(ev){
				var node = Ranger.getSelectionStart();
				if(ev.which >= 37 && ev.which <= 40){
					Ranger.save(block);
					if(Ranger.selection.text != '' && block.innerHTML != ''){
						_self.showToolbar();
					}else{
						_self.hideToolbar();
					}
				}else if(block.innerText == ''){
					block.toggle_class('placeheld');
					block.innerHTML = placeholder;
				}
			});

			var mouseUp = function(ev){
				if(_self.drag == 1){
					Ranger.save(block);
					if(Ranger.selection.text != ''){
						_self.link.value = '';
						_self.linkDialog.remove_class('on');
						_self.editbar.remove_class('expanded');
						_self.showToolbar();
					}else{
						_self.hideToolbar();
					}
				}else{
					_self.hideToolbar();
				}
			}

			block.ev('mouseup', mouseUp);

			block.focus(function(ev){
				var changed = false;
				if(_self.editBlock != block){
					_self.editBlock = block;
					changed = true;
				}

				if(block.innerHTML == placeholder){
					block.toggle_class('placeheld');
					block.innerHTML = '';
					block.focus();
				}else{
					if(!changed)
						Ranger.restore(block);
				}
			});

			block.blur(function(ev){
				_self.hideToolbar();

				if(block.innerText == ''){
					block.toggle_class('placeheld');
					block.innerHTML = placeholder;
				}
			});

			block.ev('paste', function(e) {
				e.stopPropagation();
				e.preventDefault();
				var type = _self.findType(e);
				var data = e.clipboardData.getData(type);
				_self.processpaste(block, data, type);
				return false;
			});
		}


		this.codeBlock = function(){
			console.log('New Code');
		}

		this.productBlock = function(){
			console.log('New Product');
		}

		this.dividerBlock = function(){
			var block = Coffea.create('section', {'class':'blklab-block divider'});
			block.data('type', 'divider');
			if(_self.point){
				_self.content.insertBefore(block, _self.point);
				_self.point = null;
			}else{
				_self.content.appendChild(block);
			}
		}

		this.calendarBlock = function(){
			console.log('New Calendar');
		}


		//Utils

		this.showToolbar = function() {
			if(!_self.sort.active){
				var toolbar = _self.editbar;
				var defaultLeft = 0;
				var buttonHeight = 100,
					selection = window.getSelection(),
					range = selection.getRangeAt(0),
					boundary = range.getBoundingClientRect(),
					defaultLeft = (toolbar.offsetWidth / 2),
					middleBoundary = (boundary.left + boundary.right) / 2,
					halfOffsetWidth = toolbar.offsetWidth / 2;
				if(boundary.top < buttonHeight) {
					toolbar.style.top = (buttonHeight + boundary.bottom + window.pageYOffset - toolbar.offsetHeight) + 'px';
				}else{
					toolbar.style.top = (boundary.top + window.pageYOffset - toolbar.offsetHeight) + 'px';
				}
				if(middleBoundary < halfOffsetWidth){
					toolbar.style.left = halfOffsetWidth + 'px';
				} else if ((window.innerWidth - middleBoundary) < halfOffsetWidth) {
					toolbar.style.left = window.innerWidth + defaultLeft - halfOffsetWidth + 'px';
				} else {
					toolbar.style.left = middleBoundary - halfOffsetWidth + 'px';
				}

				_self.currentTag = Ranger.getSelectionStartNode();

				_self.activateButtons();

				_self.editbar.add_class('show');
			}
			return this;
		}

		this.activateButtons = function(){
			this.editButtons.each(function(el){
				if(_self.currentTag.tagName.toLowerCase() == el.data('tag')){
					el.add_class('on');
					if(_self.currentTag.tagName.toLowerCase() == 'a'){
						_self.link.value = _self.currentTag.href;
					}
				}else if(_self.currentTag.className && _self.currentTag.className.indexOf(el.data('type')) != -1){
					el.add_class('on');
					if(_self.currentTag.tagName.toLowerCase() == 'a'){
						_self.link.value = _self.currentTag.href;
					}
				}else{
					el.remove_class('on');
				}
			});
		}

		this.stopping = false;

		this.hideToolbar = function(){
			if(_self.editbar.has_class('show') && !_self.stopping){
				_self.stopping = true;
				_self.timer = window.setInterval(function(){
					window.clearInterval(_self.timer);
					_self.editbar.remove_class('show');
					_self.stopping = false;
				}, 500);
			}else{
				window.clearInterval(_self.timer);
				_self.stopping = false;
			}
		}

		this.findType = function(e){
			if (/text\/html/.test(e.clipboardData.types)) {
				return 'text/html';
			} else if (/text\/plain/.test(e.clipboardData.types)) {
				return 'text/plain';
			} else if (/text\/rtf/.test(e.clipboardData.types)) {
				return 'text/rtf';
			}else{
				return 'text/html';
			}
		}

		this.processpaste = function(elem, savedcontent, type) {
			document.execCommand('insertHTML', false, _self.cleanHTML(savedcontent));
		}

		this.cleanHTML = function(html, exclude){
			var tmp = Coffea.create('div',{});
			var re = new RegExp("\u00a0", "g");
			tmp.innerHTML = html.trim().replace(/<!--[\s\S]*?-->/g, '').replace(re, " ");
			_self.cleanChildren(tmp.childNodes, exclude);
			var ret = tmp.innerHTML.replace(/<br>/g, '').replace(/&nbsp;/g, ' ');
			var len = tmp.children_by_tag('*').length;
			if(len <= 1){
				ret = '<p>' + ret + '</p>';
			}
			tmp.destroy();
			return ret;
		}

		this.cleanChildren = function(tmp, exclude){
			var ch = tmp;
			var arr = []
			for (var i = 0, ref = arr.length = ch.length; i < ref; i++){arr[i] = ch[i];}
			var i;
			for(i=0; i<arr.length; i++){
				var el = arr[i];
				if(el.tagName){
					if(el.tagName.toLowerCase().trim() == 'meta' || el.tagName.toLowerCase().trim() == 'link' || el.tagName.toLowerCase().trim() == 'o:p'){
						el.parentNode.removeChild(el);
					}else if(el.innerHTML == ''){
						if(el.tagName.toLowerCase().trim() != 'br' && el.tagName.toLowerCase().trim() != 'img'){
							el.parentNode.removeChild(el);
						}else{
							el.removeAttribute('style');
							if(exclude != 'class'){
								el.removeAttribute('class');
							}
							el.removeAttribute('id');
						}
					}else{
						try{
							el.removeAttribute('style');
							if(exclude != 'class'){
								el.removeAttribute('class');
							}
							el.removeAttribute('id');
						}catch(e){}
						if(el.hasChildNodes()){
							_self.cleanChildren(el.childNodes);
						}
					}
				}
			}
		}

		this.init();
	}


	window.TextBlock = function(){
		_self = this;

		this.init = function(){}

		this.init();
	}

	window.Layout = function(cls, parent, editor, callback){
		var _self = this;
		this.dragSrcEl;
		this.selector = cls;
		this.parentName = parent;
		this.editor = editor;
		this.parent;
		this.elements;
		this.enabled = false;
		this.xdirection; //0 = right; 1 = left; -1 = same;
		this.ydirection; //0 = up; 1 = down; -1 = same;
		this.lastX = 0;
		this.lastY = 0;
		this.ghost;
		this.dragStart = false;
		this.lastTarget;
		this.callback = callback;
		this.active = false;

		this.init = function(){
			this.parent = $(this.parentName);
		}

		this.toggle = function(){
			if(_self.enabled){
				_self.enabled = false;
				_self.unload();
			}else{
				_self.enabled = true;
				_self.enable();
			}
		}

		this.enable = function(){
			_self.parent.add_class('active');

			if(this.insertionPoints){
				this.insertionPoints.removeAll();
			}

			_self.update(true);
			var i;
			for(i=0;i<_self.editor.maps.length;i++){
				_self.editor.maps[i].setOptions({draggable: false});
			}

			var doc = $(document.documentElement);
			_self.active = true;
			var insertionpoints = Coffea.findAll('.blklab-insertion-point');
			insertionpoints.each(function(el){
				el.add_class('on');
			});
			_self.elements = _self.parent.children_by_class(_self.selector);
			_self.dragSrcEl = null;
			_self.elements.each(function(el){
				el.draggable = true;
			});
			_self.elements.on('dragstart', _self.start);
			_self.elements.on('drop', _self.drop);
			_self.elements.on('mouseup', _self.drop);
			doc.ev('mouseup', _self.drop);
			_self.parent.ev('mouseup', _self.drop);
			_self.parent.ev('dragover', _self.over);
			_self.parent.ev('dragenter', _self.parentEnter);
			_self.parent.ev('dragleave', _self.leave);
			_self.elements.on('dragenter', _self.enter);
			_self.elements.on('dragleave', _self.leave);
			_self.elements.on('dragover', _self.over);
			_self.elements.on('dragend', _self.end);
		}

		this.unload = function(){
			_self.parent.remove_class('active');

			var i;
			for(i=0;i<_self.editor.maps.length;i++){
				_self.editor.maps[i].setOptions({draggable: true});
			}

			var doc = $(document.documentElement);
			_self.active = false;
			var insertionpoints = Coffea.findAll('.blklab-insertion-point');
			insertionpoints.each(function(el){
				el.remove_class('on');
			});

			_self.dragSrcEl = null;
			_self.elements.each(function(el){
				el.draggable = false;
			});
			_self.elements.off('dragstart', _self.start);
			_self.elements.off('drop', _self.drop);
			_self.elements.off('mouseup', _self.drop);
			doc.off('mouseup', _self.drop);
			_self.parent.off('mouseup', _self.drop);
			_self.parent.off('dragover', _self.over);
			_self.parent.off('dragenter', _self.enter);
			_self.parent.off('dragleave', _self.leave);
			_self.elements.off('dragenter', _self.enter);
			_self.elements.off('dragleave', _self.leave);
			_self.elements.off('dragover', _self.over);
			_self.elements.off('dragend', _self.end);

		}

		this.elementsOn = function(){
			_self.elementsOff();
			_self.elements = _self.parent.children_by_class(_self.selector);

			if(_self.active){
				_self.elements.each(function(el){
					el.draggable = true;
				});
				_self.elements.on('dragstart', _self.start);
				_self.elements.on('drop', _self.drop);
				_self.elements.on('mouseup', _self.drop);
				_self.elements.on('dragenter', _self.enter);
				_self.elements.on('dragleave', _self.leave);
				_self.elements.on('dragover', _self.over);
				_self.elements.on('dragend', _self.end);
			}
		}

		this.elementsOff = function(){
			if(_self.elements){
				_self.elements.each(function(el){
					el.draggable = false;
				});
				_self.elements.off('dragstart', _self.start);
				_self.elements.off('drop', _self.drop);
				_self.elements.off('mouseup', _self.drop);
				_self.elements.off('dragenter', _self.enter);
				_self.elements.off('dragleave', _self.leave);
				_self.elements.off('dragover', _self.over);
				_self.elements.off('dragend', _self.end);
			}
		}

		this.update = function(enabled){
			_self.elementsOn();
			if(_self.active || enabled){
				this.insertionPoints = Coffea.findAll('.blklab-insertion-point');
				if(this.insertionPoints)
					this.insertionPoints.removeAll();
				this.blocks = Coffea.findAll('.blklab-block');
				var i = 0;
				var previous;
				this.blocks.each(function(el){
					i++;
					var matchUUID = previous && previous.data('uuid') ? previous.data('uuid') != el.data('uuid') :  true;
					if(!document.getElementById('point-' + i) && matchUUID){
						el.css({opacity:1})
						_self.createInsertionPoint(el, i);
					}
					previous = el;
				});
				//this.unload();
				//this.enable();
			}
		}

		this.createInsertionPoint = function(el, i, active){
			var point = Coffea.create('div', {'class': 'blklab-insertion-point', 'id':'point-' + i});
			point.innerHTML = '+ Add New Block Here...';
			if(_self.active)
				point.add_class('on');
			var menu = _self.editor.editmenu;
			var toolbar = _self.editor.toolbar;
			var layout = _self.editor.buttons.layout;
			var s = _self.editor;

			point.click(function(ev){
				s.toggleMenu(ev);
			});

			point.ev('dragenter', function(ev){
				ev.preventDefault();
				point.toggle_class('active');
			});

			point.ev('dragleave', function(ev){
				ev.preventDefault();
				point.toggle_class('active');
			});

			el.parentNode.insertBefore(point, el);
		}

		this.start = function(ev){
			ev.stopPropagation();
			ev.preventDefault();
			var target = Evnt.target(ev);
			if(!target.has_class('blklab-block')){
				target = target.parent;
			}
			ev.dataTransfer.effectAllowed = 'move';
			ev.dataTransfer.setData('text/html', this.innerHTML);
			_self.dragSrcEl = target;
			this.style.opacity = '0.5';
			this.lastX = ev.clientX;
			this.lastY = ev.clientY;
			_self.dragStart = true;
			target.css({
				opacity:0.1
			});

			//_self.elements.on('mousemove', _self.move);
			var doc = $(document.documentElement);
			doc.ev('mousemove', _self.move);
		}

		this.move = function(ev){
			ev.stopPropagation();
			ev.preventDefault();
			var element = document.elementFromPoint(ev.clientX, ev.clientY);
			if(_self.dragStart && element != _self.dragSrcEl){

				if(_self.lastTarget != element && _self.lastTarget != element.parent){
					_self.lastTarget.remove_class('blklab-above');
					_self.lastTarget.remove_class('blklab-below');
					_self.lastTarget.remove_class('blklab-right');
					_self.lastTarget.remove_class('blklab-left');
					_self.lastTarget.remove_class('active');
				}

				element = _self.lastTarget != element ? $(element) : _self.lastTarget;

				if(element.parent.has_class('blklab-block')){
					element = element.parent;
				}
				if(element.has_class('blklab-block')){
					if(_self.isLeft(element, ev)){
						element.add_class('blklab-left');
						element.remove_class('blklab-right');
						element.remove_class('blklab-below');
						element.remove_class('blklab-above');
					}else if(_self.isMiddle(element, ev)){
						if(_self.isAbove(element, ev)){
							element.add_class('blklab-above');
							element.remove_class('blklab-below');
							element.remove_class('blklab-right');
							element.remove_class('blklab-left');
						}else{
							element.add_class('blklab-below');
							element.remove_class('blklab-above');
							element.remove_class('blklab-right');
							element.remove_class('blklab-left');
						}
					}else if(_self.isRight(element, ev)){
						element.add_class('blklab-right');
						element.remove_class('blklab-left');
						element.remove_class('blklab-below');
						element.remove_class('blklab-above');
					}
				}else if(element.has_class('blklab-insertion-point')){
					element.add_class('active');
				}
			}

			_self.lastTarget = element;
		}

		this.drop = function(ev){
			ev.stopPropagation();
			ev.preventDefault();

			_self.removeSpacers();

			var element = document.elementFromPoint(ev.clientX, ev.clientY);
			if(_self.dragStart && element != _self.dragSrcEl && element.tagName.toLowerCase() != 'body' && element.id != 'content'){
				element = _self.lastTarget != element ? $(element) : _self.lastTarget;
				if(element.parent.has_class('blklab-block')){
					element = element.parent;
				}
				if(element.has_class('blklab-block')){
					if(_self.isLeft(element, ev)){
						_self.addLeft(element);
					}else if(_self.isMiddle(element, ev)){
						if(_self.isAbove(element, ev)){
							_self.addAbove(element);
						}else{
							_self.addBelow(element);
						}

					}else if(_self.isRight(element, ev)){
						_self.addRight(element);
					}

					_self.lastTarget = element;

					_self.update();
				}else if(element.has_class('blklab-insertion-point')){
					_self.addBelow(element);
					_self.lastTarget = element;
					_self.update();
				}else{
					if(_self.dragSrcEl.data('type') == 'image'){
						var img = _self.dragSrcEl.children_by_tag('IMG')[0];
						element.parent.insertBefore(img, element);
						_self.dragSrcEl.destroy();
					}
					_self.lastTarget = element;
				}
			}else if(element.id == 'content'){
				var tmp = _self.dragSrcEl;
				var uuid = tmp.data('uuid');
				var pairs = Coffea.findAll('[data-uuid="' + uuid + '"]');
				if(pairs.length == 2){
					pairs.each(function(el){
						el.remove_data('uuid');
						el.preg_remove_class('col-.*?-.*?');
					});
				}else{
					tmp.remove_data('uuid');
					tmp.preg_remove_class('col-.*?-.*?');

					pairs = Coffea.findAll('[data-uuid="' + uuid + '"]');
					var cnt = pairs.length;
					cls = 'col-1-' + cnt;
					pairs.each(function(el){
						el.preg_remove_class('col-.*?-.*?');
						el.add_class(cls);
					});
				}
				_self.dragSrcEl.destroy();
				element.append(tmp);
			}else if(element.tagName.toLowerCase() == 'body'){
				var tmp = _self.dragSrcEl;
				var uuid = tmp ? tmp.data('uuid') : '';
				var pairs = Coffea.findAll('[data-uuid="' + uuid + '"]');
				if(pairs.length == 2){
					pairs.each(function(el){
						el.remove_data('uuid');
						el.preg_remove_class('col-.*?-.*?');
					});
				}else{
					if(tmp)
						tmp.remove_data('uuid');
					tmp.preg_remove_class('col-.*?-.*?');

					pairs = Coffea.findAll('[data-uuid="' + uuid + '"]');
					var cnt = pairs.length;
					cls = 'col-1-' + cnt;
					pairs.each(function(el){
						el.preg_remove_class('col-.*?-.*?');
						el.add_class(cls);
					});
				}
				_self.dragSrcEl.destroy();
			}

			_self.elements.each(function(el){
				el.css({
					opacity:1
				});
			});

			_self.end(ev);
		}

		this.isMiddle = function(element, ev){
			var bounds = element.bounds();
			var widthPercent = bounds.w * 0.33333333;
			var xboundary1 = widthPercent;
			var xboundary2 = widthPercent * 2;
			var xboundary3 = widthPercent * 3;
			var relativeX = ev.clientX - bounds.x;
			return relativeX > xboundary1 && relativeX <= xboundary2
		}

		this.isAbove = function(element, ev){
			var bounds = element.bounds();
			var heightPercent = bounds.h * 0.5;
			var yboundary1 = heightPercent;
			var relativeY = ev.clientY - bounds.y;
			return relativeY < yboundary1;
		}

		this.isLeft = function(element, ev){
			var bounds = element.bounds();
			var widthPercent = bounds.w * 0.33333333;
			var xboundary1 = widthPercent;
			var xboundary2 = widthPercent * 2;
			var xboundary3 = widthPercent * 3;
			var relativeX = ev.clientX - bounds.x;
			return relativeX <= xboundary1;
		}

		this.isRight = function(element, ev){
			var bounds = element.bounds();
			var widthPercent = bounds.w * 0.33333333;
			var xboundary1 = widthPercent;
			var xboundary2 = widthPercent * 2;
			var xboundary3 = widthPercent * 3;
			var relativeX = ev.clientX - bounds.x;

			return relativeX > xboundary2;
		}

		this.addAbove = function(element){
			try{
				var tmp = _self.dragSrcEl;
				_self.dragSrcEl.destroy()

				_self.removeOld(tmp);
				_self.addNewRow(element, tmp);

				_self.parent.insertBefore(tmp, element);
			}catch(e){}

			_self.update();
		}

		this.addBelow = function(element){
			try{
				if(element.nextSibling != null){
					var tmp = _self.dragSrcEl;
					_self.dragSrcEl.destroy()

					_self.removeOld(tmp);
					_self.addNewRow(element, tmp);


					_self.parent.insertBefore(tmp, element.nextSibling);
				}else{
					var tmp = _self.dragSrcEl;
					_self.removeOld(tmp);
					_self.dragSrcEl.destroy()
					_self.parent.append(tmp);
				}
			}catch(e){
				console.log(e);
			}
			_self.update();
		}

		this.addLeft = function(element){
			try{
				var tmp = _self.dragSrcEl;
				_self.dragSrcEl.destroy()
				var cls;

				_self.removeOld(tmp);
				_self.addNew(element, tmp);
				_self.parent.insertBefore(tmp, element);
			}catch(e){
				console.log(e);
			}
			_self.update();
		}

		this.addRight = function(element){
			try{
				var tmp = _self.dragSrcEl;
				_self.dragSrcEl.destroy()
				var cls;

				_self.removeOld(tmp);
				_self.addNew(element, tmp);

				if(element.nextSibling != null){
					var tmp = _self.dragSrcEl;
					_self.dragSrcEl.destroy()
					_self.parent.insertBefore(tmp, element.nextSibling);
				}else{
					var tmp = _self.dragSrcEl;
					_self.dragSrcEl.destroy()
					_self.parent.append(tmp);
				}
			}catch(e){}

			_self.update();
		}

		this.addNew = function(element, tmp){
			var uuid;
			if(element.data('uuid')){
				var pairs = Coffea.findAll('[data-uuid="' + element.data('uuid') + '"]');
				var cnt = pairs.length + 1;
				uuid = element.data('uuid')
				cls = 'col-1-' + cnt;
				tmp.add_class(cls);
				pairs.each(function(el){
					el.preg_remove_class('col-.*?-.*?');
					el.add_class(cls);
				});
			}else{
				uuid = Coffea.Utils.generateUUID();
				cls = 'col-1-2';
				tmp.add_class(cls);
				element.add_class(cls);
			}

			tmp.data('uuid', uuid);
			element.data('uuid', uuid);
		}

		this.addNewRow = function(element, tmp){
			var pairs = Coffea.findAll('[data-uuid="' + element.data('uuid') + '"]');
			if(pairs.length == 2){
				pairs.each(function(el){
					el.remove_data('uuid');
					el.preg_remove_class('col-.*?-.*?');
				});
			}else{
				pairs = Coffea.findAll('[data-uuid="' + element.data('uuid') + '"]');
				var cnt = pairs.length;
				cls = 'col-1-' + cnt;
				pairs.each(function(el){
					el.preg_remove_class('col-.*?-.*?');
					el.add_class(cls);
				});
			}
		}

		this.removeOld = function(tmp){
			var olduuid = tmp.data('uuid');
			tmp.remove_data('uuid');
			tmp.preg_remove_class('col-.*?-.*?');

			var oldpairs = Coffea.findAll('[data-uuid="' + olduuid + '"]');
			var cnt = oldpairs.length;
			cls = 'col-1-' + cnt;
			oldpairs.each(function(el){
				el.preg_remove_class('col-.*?-.*?');
				el.add_class(cls);
			});
		}

		this.end = function(ev){
			ev.stopPropagation();
			ev.preventDefault();
			_self.removeSpacers(true);
			//_self.elements.off('mousemove', _self.move);
			var doc = $(document.documentElement);
			doc.off('mousemove', _self.move);
			_self.dragStart = false;
			_self.elements.each(function(el){
				el.css({
					opacity:1
				});
			});
		}

		this.parentEnter = function(ev){
			ev.stopPropagation();
			ev.preventDefault();
			console.log('Parent ' + this);
			return false;
		}

		this.enter = function(ev){
			ev.stopPropagation();
			ev.preventDefault();
			console.log(this);
			return false;
		}

		this.leave = function(ev){
			ev.stopPropagation();
			ev.preventDefault();
			console.log(this);
			_self.removeSpacers();
			return false;
		}

		this.over = function(ev){
			ev.stopPropagation();
			ev.preventDefault();
			console.log(this);
			ev.dataTransfer.dropEffect = 'move';
			return false;
		}

		this.removeSpacers = function(resetOpacity){
			_self.elements.each(function(el){
				if(resetOpacity){
					el.css({
						opacity:1
					});
				}
				el.remove_class('blklab-right');
				el.remove_class('blklab-left');
				el.remove_class('blklab-below');
				el.remove_class('blklab-above');
			});
		}

		this.init();
	}

})();
