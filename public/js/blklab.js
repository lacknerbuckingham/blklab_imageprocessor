//Todo off function

(function (window, document) {
	'use strict'

	Function.prototype.on = function () {
		this.__on__ = Array.prototype.slice.call(arguments);
		return this;
	};

	window.BlkLab = function(){}

	BlkLab.DataBind = {
		bound: {},

		fire: function(flag, ev){
			if(this.bound[flag]){
				var i;
				var elements = this.bound[flag];
				var len = this.bound[flag].length;
				for(i=0;i<len;i++){
					elements[i].call(ev);
				}
			}
		},

		on: function(flag, callback){
			var bound = this.bound;
			if(bound[flag]){
				bound[flag].push(callback);
			}else{
				bound[flag] = [];
				bound[flag].push(callback);
			}
			return this;
		},

		off: function(){}
	}

	BlkLab.extend = function(obj, methods) {
		var method;
		for(method in methods) {
			if(obj && obj.hasOwnProperty(method) === false) {
				obj[method] = methods[method];
			}
		}
		return obj;
	};

	BlkLab.on = function(obj, method){
		var o = $(obj);
		var prop = method.__on__;
		if(o)
			o.on(prop, method);
	}

	BlkLab.DOM = {
		registry: {},

		find: function(selector){
			var ele;
			if(selector in this.registry){
				console.log('cached');
				ele = this.registry[selector];
			}else{
				if(typeof selector === 'string' || (selector[0] == '#')) {
					if(typeof selector !== 'object') {
						ele = document.querySelector(selector);
						if(!ele) {
							ele = document.getElementById(selector);
						}
					}else{
						ele = selector;
					}
				}else if(typeof selector === 'object'){
					ele = selector;
				}
				this.registry[selector] = ele;
			}

			BlkLab.extend(ele, {
				listeners: {},

				off: function (type, fn) {
					if(window.removeEventListener) {
						this.removeEventListener(type, fn, false);
					} else if (window.detachEvent) {
						this.detachEvent('on' + type, fn);
					}
				},

				on: function(type, fn){
					if (!this.listeners[type]) {
						this.listeners[type] = [];
					}
					this.listeners[type].push(fn);

					this.off(type, fn);

					if (window.addEventListener) {
						this.addEventListener(type, fn, false);
					} else if (window.attachEvent) {
						this.attachEvent('on' + type, fn);
					}

					return this;
				}
			});
			return ele;
		},

		findAll: function(selector){
			var ele = {};
			ele.data = [];
            var col = document.querySelectorAll(selector);
            if(col.length > 0) {
                var i;
                for (i = 0; i < col.length; i++) {
                    var el = this.find(col[i]);
                    ele.data.push(el);
                }
            }

			BlkLab.extend(ele, {
				on: function(type, fn){
					var i;
					for(i=0;i<ele.data.length;i++){
						ele.data[i].on(type, fn);
					}
				},
			});
			return ele;
		}
	}

	window.$ = function(selector){
		var first = selector[0];
		var last = selector[selector.length-1];
		if(first == '.' || first == '[' && last == ']'){
			return BlkLab.DOM.findAll(selector);
		}else{
			return BlkLab.DOM.find(selector);
		}
	}

}(window, document));
