(function () {
	//Models
	BlkLab.App.pagesModel = BlkLab.Model.extend();
	BlkLab.App.pagesModel.url = '/api/pages';

	//Views
	BlkLab.App.pagesView = BlkLab.View.extend({
		template: '<h1>{{title}}</h1>',

		render: function(el){
			var self = this;
			var template = Handlebars.compile(this.template);
			var html = template(this.model.data);
			var ele = document.querySelector(el);
			ele.innerHTML = html;
		}
	});

	//Controllers
	BlkLab.App.pagesController = BlkLab.Controller.extend({
		isEditing: false,

		actions: {
		},

		render: function(route){
			BlkLab.App.pagesModel.find({id: route}).then(function(model){
				var view = new BlkLab.App.pagesView();
				view.model = model;
				view.render('#content');
			});
		}
	});

	BlkLab.App.setupController = BlkLab.Controller.extend({
		actions: {
			nextStep: function(e){
				console.log(this);
			},

			hoverAction: function(e){
				console.log(this);
			}
		},

		render: function(route){}
	});

	/*Routes
	BlkLab.App.Router.route('default', {
		controller:BlkLab.App.pagesController
	});*/

	BlkLab.App.Router.routes({
		'/setup': {
			controller: BlkLab.App.setupController
		},

		default: {
			controller:BlkLab.App.pagesController
		}
	});

	BlkLab.History.start();
	BlkLab.App.run();

})();
