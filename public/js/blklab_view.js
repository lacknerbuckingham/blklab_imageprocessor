BlkLab.View = function(){}

BlkLab.View.prototype = {
	template: '',

	init: function(){},

	model: {},

	render: function(){
		console.log('base');
	}
}

BlkLab.View.extend = function(methods){
	var self =  function(){
		BlkLab.View.call(this);
	}

	self.prototype = Object.create(BlkLab.View.prototype);
	self.prototype.constructor = self;

	for(method in methods){
		if(self.prototype.hasOwnProperty(method) === false) {
			self.prototype[method] = methods[method];
		}
	}
	return self;
}
