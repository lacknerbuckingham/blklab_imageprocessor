#!/bin/sh                        


FFMPEG=/usr/local/bin/ffmpeg
HD_SUFFIX='_hd'
EMBED_WIDTH='640'
EMBED_HEIGHT='360'
SD_RESOLUTION=$EMBED_WIDTH'x'$EMBED_HEIGHT
SERVER_VIDEO_PATH='http://www.example.xx/media/video'
 
DESCR_H264='mp4 (h.264/aac)'
DESCR_WEBM='webm (vp8/vorbis)'
DESCR_OGG='ogv (theora/vorbis)'

DIR=/var/www/processor/public/assets/videos
INFILE=$DIR/$1

$FFMPEG -i $INFILE -vframes 1 -map 0:v:0 -vf "scale=100:100" $DIR/$2-thumb.png
$FFMPEG -i $INFILE -vframes 1 -pix_fmt yuv420p $DIR/$2.png
$FFMPEG -i $DIR/$1 -c:v libx264 -pix_fmt yuv420p $DIR/$2.mp4
$FFMPEG -i $DIR/$1 -c:v libvpx -pix_fmt yuv420p -qmin 0 -qmax 25 -crf 10 -b:v 2M -c:a libvorbis -q:a 4 -vf "scale=trunc(in_w/2)*2:trunc(in_h/2)*2" $DIR/$2.webm
#ffmpeg -i $DIR/$1 -c:v libtheora -pix_fmt yuv420p -q 18 -qmin 0 -qmax 20 -crf 10 -b:v 2M -c:a libvorbis -q:a 4 -vf "scale=trunc(in_w/2)*2:trunc(in_h/2)*2" $DIR/$2.ogv


#$FFMPEG -i $DIR/$1 -vcodec libx264 -pix_fmt yuv420p -profile:v baseline -preset slower -crf 18 -vf "scale=trunc(in_w/2)*2:trunc(in_h/2)*2" $DIR/$2.mp4
#$FFMPEG -i $INFILE -b 3800k -vcodec libvpx -acodec libvorbis -ab 160000 -f webm -g 30 $DIR/$2.webm
#$FFMPEG -i $INFILE -b 3800k -vcodec libtheora -acodec libvorbis -ab 160000 -g 30 $DIR/$2.ogv
