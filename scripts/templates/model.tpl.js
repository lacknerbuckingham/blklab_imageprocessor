var Model = require('blklab').Model;
var util = require('util');

var [[module]]Model = Model.extend({});

var model = new [[module]]Model({
	db:'[[dbname]]',
	collection: '[[module]]'
});

model.schema({
	title: {type: 'String'},
	identifier: {type: 'String'},
	deleted: {type: 'Integer', 'default': 0}
});

module.exports = model;
